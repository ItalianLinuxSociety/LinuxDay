<?php

$internal_current_date_do_not_read = date('Y-m-d');

date_default_timezone_set("Europe/Rome");

$current_year = '2024';
$computer_date = '2024-10-26';
$shipping_date = '2024-10-11';
$human_date = 'Sabato 26 Ottobre 2024';
$administrators = ['bob@linux.it', 'gnu@linux.it', 'direttore@linux.it'];

$is_virtual  = false;
$is_physical = true;

$sessions = [];

$talks_date = '2024-09-27';
$human_talks_date = 'Venerdi 27 Settembre';

// alphabetical order
// Logo please 180px x 180px
$sponsors = [
	'Cloud68' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/cloud68.png',
		'link' => 'https://cloud68.co/',
	],
	'Continuity' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/continuity.png',
		'link' => 'https://continuity.space/',
	],
	'Extraordy' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/extraordy.png',
		'link' => 'https://www.extraordy.com/',
	],
	'Linux Professional Institute' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/lpi.png',
		'link' => 'https://www.lpi.org/it/',
	],
	'Red Hat' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/redhat.png',
		'link' => 'https://www.redhat.com/it',
	],
	'SUSE' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/suse.png',
		'link' => 'https://www.suse.com/',
	],
];

$supporters = [
	'GARR' => (object) [
		'logo' => '/immagini/garr.png',
		'link' => 'https://garr.it/',
	],
];

$patronages = [];

$theme = [];

$ils_logo = 'immagini/ils-circle-200.png';

// This is only shown in the page /year/condividi/
$plaintext_direct_contacts = <<<EOF
- Valerio Bozzolan (Presidente Italian Linux)
  Telefono: 348 702 7731 (mandatemi pure un SMS o chiamatemi)
  Email: presidente@linux.it
  Telegram: @bozzy

- Fabio Lovato (Direttore)
  Email: direttore@linux.it
  Telegram: @loviuz
  Mastodon: https://mastodon.uno/@loviuz86
EOF;

$press_review = [
	[
		'url' => 'https://www.unime.it/eventi/il-linux-day-2024-al-dipartimento-di-ingegneria',
		'title' => 'Il Linux Day 2024 al Dipartimento di Ingegneria di Messina',
		'date' => '2024-10-26'
	],
	[
		'url' => 'https://www.unionesarda.it/news-sardegna/nuoro-provincia/macomer-sabato-il-linux-day-giornata-nazionale-per-il-software-libero-ijqhvv66',
		'title' => 'Unione Sarda: il Linux Day a Macomer',
		'date' => '2024-10-24'
	],

	[
        'url' => 'https://www.radiobullets.com/rubriche/linux-day-sabato-26-ottobre-in-tutta-italia/',
        'title' => 'Radio Bullets: Linux Day sabato 26 ottobre in tutta Italia',
        'date' => '2024-10-26'
    ],
    [
        'url' => 'https://www.youtube.com/watch?v=anGHBEbWe9c',
        'title' => 'TGR Sicilia: Linux Day',
        'date' => '2024-10-27'
    ],
    [
        'url' => 'https://www.palermotoday.it/attualita/linux-day-2024-efficienza-energetica-26-ottobre-2024.html',
        'title' => 'Palermo Today: Linux Day 2024 e efficienza energetica',
        'date' => '2024-10-26'
    ],
    [
        'url' => 'https://www.ferraratoday.it/eventi/linux-day-2024-info-programma.html',
        'title' => 'Ferrara Today: anche a Ferrara un evento per la giornata nazionale a sostegno del software libero',
        'date' => '2024-10-24'
    ],
    [
        'url' => 'https://www.tvsette.net/linux-day-2024-unisciti-a-noi-per-una-giornata-di-innovazione-tecnologica/',
        'title' => 'TV Sette: Unisciti a Noi per una Giornata di Innovazione Tecnologica!',
        'date' => '2024-10-22'
    ],
    [
        'url' => 'https://www.imperiapost.it/713991/imperia-torna-il-linux-day-2024-sabato-26-ottobre-appuntamento-allarci-il-campo-delle-fragole',
        'title' => 'Imperia Post: appuntamento all\'ARCI, Il Campo delle Fragole',
        'date' => '2024-10-22'
    ],
    [
        'url' => 'https://www.ecodibergamo.it/stories/premium/Economia/linux-day-giornata-allinsegna-dellinnovazione-bergamo-o_2585016_11/',
        'title' => 'L\'eco di Bergamo: giornata all\'insegna dell\'innovazione a Bergamo',
        'date' => '2024-10-26'
    ],
    [
        'url' => 'https://www.bresciatoday.it/eventi/fiere/brescia-linux-day.html',
        'title' => 'Brescia Today: Linux Day',
        'date' => '2024-10-21'
    ],
    [
        'url' => 'https://www.rietilife.com/2024/10/12/linux-day-2024-a-rieti-entra-nel-mondo-di-linux-e-dellopen-source/',
        'title' => 'Rieti Life: entra nel mondo di Linux e dell\'Open Source!',
        'date' => '2024-10-12'
    ],
    [
        'url' => 'https://www.massa-critica.it/2024/10/sabato-26-ottobre-2024-torna-il-linux-day/',
        'title' => 'Massa Critica: Sabato 26 Ottobre 2024 torna il Linux Day',
        'date' => '2024-10-26'
	],
	[
        'url' => 'https://www.canalesicilia.it/linux-e-cybersecurity-evento-alliis-verona-trento-di-messina/',
        'title' => 'Canale Sicilia: Linux e Cybersecurity, evento all\'IIS di Verona Trento di Messina',
        'date' => '2024-10-24'
    ],
    [
        'url' => 'https://www.gazzettadimilano.it/economia/nuove-tecnologie/linux-day-a-cura-di-rigenerami/',
        'title' => 'Gazzetta di Milano: Linux Day a cura di RigeneraMi',
        'date' => '2024-10-14'
    ],
    [
        'url' => 'https://www.aronanelweb.it/2024/10/26/linux-day-2024-ad-arona-con-jet-lug/',
        'title' => 'Arona nel Web:Linux Day 2024 ad Arona con Jet Lug',
        'date' => '2024-10-26'
    ],
    [
        'url' => 'https://www.labtv.net/attualita/2024/10/28/linux-day-un-successo-per-linnovazione-e-la-cultura-libera/',
        'title' => 'Lab TV: Linux Day, un successo per l\'innovazione e la cultura libera',
        'date' => '2024-10-28'
    ],
    [
        'url' => 'https://www.pisatoday.it/formazione/ipsia-pontedera-linux-day-2024.html',
        'title' => 'Pisa Today: Linux Day 2024 all\'IPSIA di Pontedera',
        'date' => '2024-10-28'
    ],
    [
        'url' => 'https://www.messinatoday.it/eventi/linux-day-2024-dipartimento-ingegneria.html',
        'title' => 'Messina Today: Linux Day, tappa al Dipartimento di Ingegneria',
        'date' => '2024-10-25'
    ],
    [
        'url' => 'https://servizi-scandicci.055055.it/rete-civica/software-libero-linux-day-arriva-alla-biblioteca-di-scandicci',
        'title' => 'Comune di Scandicci: Software Libero, "Linux Day" arriva alla Biblioteca di Scandicci',
        'date' => '2024-10-22'
    ],
    [
        'url' => 'https://orvietosi.it/2024/10/linux-day-24-mappiamo-linux-e-il-software-libero/',
        'title' => 'OrvietoSì: Linux Day \'24: "Mappiamo Linux e il software libero"',
        'date' => '2024-10-26'
    ],
    [
        'url' => 'https://www.retechiara.it/listituto-morselli-di-gela-celebra-il-linux-day-evento-dedicato-al-software-libero',
        'title' => 'Rete Chiara: l\'Istituto Morselli di Gela celebra il Linux Day',
        'date' => '2024-10-24'
    ],
    [
        'url' => 'https://www.co-municare.it/news/sabato-26-ottobre-2024-dalle-1100-alle-1700-hacklab-eboli-e-lieto-di-invitarvi-al-linux-day-2024-che-si-terra-presso-bix-incubator-in-via-cefalonia-1-eboli/',
        'title' => 'Co-municare: Linux Day 2024 a da Hacklab Eboli presso il Bix Incubator',
        'date' => '2024-10-23'
    ],
    [
        'url' => 'https://www.lavocediimperia.it/2024/10/23/leggi-notizia/argomenti/eventi-7/articolo/a-imperia-torna-il-linux-day-programmatori-frenano-lavanzata-dellintelligenza-artificiale.html',
        'title' => 'La Voce di Imperia: a Imperia torna il Linux Day, programmatori frenano l\'avanzata dell\'Intelligenza Artificiale',
        'date' => '2024-10-23'
    ],

	[
        'url' => 'https://www.ecodibergamo.it/eventi/eppen/dettaglio/scienza/bergamo/linux-day-bergamo_243579/',
        'title' => 'L\'eco di Bergamo: Linux Day a Bergamo',
        'date' => '2024-10-26'
    ],
    [
        'url' => 'https://www.ecodibergamo.it/stories/premium/Economia/linux-day-giornata-allinsegna-dellinnovazione-bergamo-o_2585016_11/',
        'title' => 'Linux Day: Giornata all\'insegna dell\'innovazione a Bergamo',
        'date' => '2024-10-26'
    ],
    [
        'url' => 'https://www.youtube-nocookie.com/embed/1fJKauWwTRY',
        'title' => 'Bergamo TG (testo scorrevole al minuto 6:26)',
        'date' => '2024-10-28'
    ],
    [
        'url' => 'https://www.rietilife.com/2024/11/06/grande-successo-per-il-linuxday-2024-a-rieti-tecnologia-libera-e-partecipazione/',
        'title' => 'Rieti Life: grande successo per il Linux Day 2024 a Rieti. Tecnologia libera e partecipazione',
        'date' => '2024-11-06'
    ]
];
