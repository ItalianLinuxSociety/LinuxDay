<?php
/*
Copyright (C) 2019-2024  Italian Linux Society e contributori https://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * COMUNICATO STAMPA LINUX DAY NAZIONALE
 * IN FUTURO AGGIORNATELO PURE TOTALMENTE A CASO!
 * SENTITEVI LIBERI DI CAMBIARE TOTALMENTE LA STRUTTURA!
 * Al momento è pensato per essere abbastanza auto-aggiornante,
 * poiché è facile che in futuro ci scorderemo di scriverlo
 * da capo, o mantenerlo aggiornato. lol
 *
 * Funzionalità attuali:
 * - nel corpo, menziona le città approvate (aggiornate)
 * - dice la data giusta, sempre
 */

require_once ('../funzioni.php');

// This page never existed before 2024 :D
// It's probably better to do not generate for older pages.
if( conf('current_year') < 2024 ) {
	http_response_code(404);
	exit;
}

lugheader ('Linux Day ' . conf('current_year') . ': Condividi / Informazioni per la Stampa');

$current_year = conf('current_year');
$human_date = conf('human_date');
$plaintext_direct_contacts = conf('plaintext_direct_contacts');
$members = 300;

// Get all approved events in this year.
$events = (new LinuxDayEventCollection())
  ->query()
  ->whereIsApproved()
  ->get();

// Generate a nice phrase like "more than 20 events" if we have 21 events.
// Feel free to drop this in the future.
$n_events = count($events);
$n_events--; // "More than X cities!" lol
$n_events_circa = $n_events - ($n_events % 5);
$n_events_human = $n_events_circa > 10
  ? "diverse città d'Italia"
  : sprintf( "<b>più di %d</b> città d'Italia", $n_events_circa );
?>

<h2>Informazioni per la Condivisione e per la Stampa</h2>

In questa pagina trovi tutte le informazioni per condividere velocemente il Linux Day.

<h3 id="comunicato-stampa" style="margin-top: 1em">Comunicato Stampa per Linux Day nazionale</h3>
Questo comunicato stampa è liberamente condivisibile e adattabile. Grazie per la condivisione:

<div class="card">
  <div class="card-body">
  	<?= "L'evento <b>Linux Day</b> 2024 si terrà <b>$human_date</b> in $n_events_human!" ?>
  </div>
</div>

<div style="margin-top: 2em; margin-left: 1em; margin-bottom: 2em; border-left: 3px solid gray; padding-left: 1em"><?php
$txt = <<<EOF
L'evento tecnologico <b>Linux Day $current_year</b> focalizzato sulle tecnologie Open Source e sul Software Libero si terrà <b>$human_date</b> in $n_events_human. Questo evento totalmente gratuito si svolge da piú di 20 anni, ed é coordinato dall'associazione nazionale di volontariato <a href="https://www.ils.org/">Italian Linux Society</a> fondata nel 1994 e con più di $members soci attivi; in collaborazione con <a href="https://lugmap.linux.it/">numerosi Linux Users Group</a> (LUG), ovvero gruppi di persone volontarie impegnate a promuovere questi argomenti tecnologici, e dediti alla condivisione di Linux e del software libero in quanto soluzione etica e pratica, per non dipendere da soluzioni "proprietarie" e non dipendere dalle solite grandi aziende tecnologiche.

Ma cos'è Linux e perché è importante? Linux è un componente software essenziale, libero e open source, alla base del funzionamento di miliardi di dispositivi: dai telefoni, tablet, smart TV, computer, server, router, access point Wi-Fi, fino a <u>tutti</u> i più potenti supercomputer. Essere "software libero" significa una cosa ben precisa: il software deve poter essere usato, studiato, corretto, migliorato e distribuito liberamente, senza restrizioni di copyright, ed è il motivo per cui il software libero è dappertutto, anche se spesso non lo notiamo.

Le comunità del Software Libero, e dell'Open Source, sono state rivoluzionarie per la loro direzione contraria al "<b>tutti i diritti riservati</b>" o altre obsolete diciture che ostacolano la collaborazione e impediscono o rallentano lo studio e la correzione dei problemi digitali.

L'Open, non solo promuove la trasparenza e la collaborazione tecnologica, ma offre altri vantaggi significativi:

- Libertà e Controllo:
  gli utenti che usano software "Open" hanno il pieno controllo sui propri dispositivi e sui software che utilizzano, senza che invece sia un'altra azienda ad avere tale totale controllo sui loro dispositivi. Non c'è una limitazione all'accesso, o raccolta di dati personali potenzialmente indiscriminata. L'uso è garantito, non c'è bisogno di "chiavi di licenze" o di "piratare" o "craccare" nulla.

- Sicurezza e Privacy:
  I software Open Source, come Linux, sono spesso considerati molto più sicuri, poiché il codice sorgente è esaminato da un vastissimo numero di persone esperte e totalmente indipendenti fra loro. Questo riduce drasticamente il rischio che una vulnerabilità rimanga nascosta e non corretta per anni. Queste verifiche indipendenti garantiscono una maggiore protezione della privacy. Nessun altro software "chiuso" al mondo riceve aggiornamenti di sicurezza in modo altrettanto veloce come fa Linux.

- Costi Ridotti:
  La maggior parte delle "distribuzioni Linux" è totalmente gratuita, permettendo a privati, aziende e pubbliche amministrazioni di risparmiare sui costi di licenza software, senza compromettere la qualità. Le pubbliche amministrazioni in Italia (scuole, uffici, ecc.) fra l'altro hanno già obblighi legali di dare priorità all'adozione di software libero (art. 68 del Codice Amministrazione Digitale), per ridurre i costi e condividere le soluzioni con altre amministrazioni.

- Alternativa alle "Big Tech":
  Scegliere soluzioni totalmente "Open" significa sostenere un ecosistema di totale rispetto degli utenti. Questo evita che una singola azienda abbia il totale controllo dei suoi utenti e possa manipolare le loro abitudini a livello impensabili. In ogni settore, è necessario promuovere l'Open, per un'informatica più equa, accessibile e sostenibile.

Durante l'evento Linux Day, i partecipanti potranno conoscere la community, assistere a conferenze, laboratori e dimostrazioni pratiche, scoprendo come Linux e il software libero possano migliorare la nostra esperienza digitale. Questo evento si svolge da più di 20 anni ed è una tradizione imperdibile per chiunque voglia esplorare nuove tecnologie e apprendere come utilizzare strumenti che rispettano la libertà e la privacy degli utenti, o condividere le proprie esperienze.

Per scoprire le città e le attività in programma, basta visitare il sito ufficiale all'indirizzo linuxday.it da settembre.

Si segnala che il nostro account social ufficiale è presente anche sul social network libero Mastodon:
<a href="https://mastodon.uno/@ItaLinuxSociety">https://mastodon.uno/@ItaLinuxSociety</a>

Chi partecipa potrà condividere le foto migliori sul social Mastodon, ma anche sugli altri social tradizionali come Facebook e Twitter:
<a href="https://www.facebook.com/LinuxDayItalia">https://www.facebook.com/LinuxDayItalia</a>
<a href="https://twitter.com/LinuxDayItalia">https://twitter.com/LinuxDayItalia</a>

<b>Sito ufficiale</b> con tutte le informazioni dell'evento e con l'elenco delle città aderenti:
<b><a href="https://www.linuxday.it/">www.LinuxDay.it</a></b>

Contatti Diretti:

$plaintext_direct_contacts

- Consiglio Direttivo Italian Linux Society
  Email: ils-cd@linux.it

- Altri contatti
  https://www.ils.org/contatti/

(Comunicato per l'evento Linux Day che si terrà <b>$human_date</b> in diverse città italiane)
EOF;
echo nl2br( $txt );
?>
</div>

<h3 id="materiali">Materiali</h3>

<p>
  In questa pagina trovi una raccolta di loghi e altri materiali:<br />
  <a href="https://gitlab.com/ItalianLinuxSociety/Materiali">materiali grafici Linux.it</a>
</p>

<hr />

<p>Grazie per la condivisione dell'evento. Puoi approfondire anche le nostre linee guida con informazioni su come è svolto ogni Linux Day:</p>
<p><a href="<?php echo makeurl('/lineeguida/') ?>" class="btn btn-secondary">Linee Guida</a></p>

<?php
lugfooter ();

