<?php

$internal_current_date_do_not_read = date('Y-m-d');

date_default_timezone_set("Europe/Rome");
$current_year = '2021';
$computer_date = '2021-10-23';
$shipping_date = '2021-10-09';
$human_date = 'Sabato 23 Ottobre 2021';
$administrators = ['bob@linux.it', 'ferdi.traversa@gmail.com'];

$is_virtual = true;
$is_physical = true;

$sessions = [
    'personal' => (object) [
        'label' => 'Dati... Personali',
        'desc' => 'Divulgazione, introduzione a concetti e strumenti base',
        'player' => 'https://garr.tv/s/616e7b333ef1a19c2624b83f?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-24',
    ],
    'public' => (object) [
        'label' => 'Dati... Pubblici',
        'desc' => 'Framework, librerie, esperienze, risorse per sviluppatori',
        'player' => 'https://garr.tv/s/616e7d283ef1a126f824b840?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-24',
    ],
    'elaborated' => (object) [
        'label' => 'Dati... Elaborati',
        'desc' => 'Talk generici o di approfondimento su applicazioni libere',
        'player' => 'https://garr.tv/s/616e7df13ef1a12bca24b841?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-24',
    ],
    'other' => (object) [
        'label' => 'Approfondimenti',
        'desc' => 'Interventi trasversali di approfondimento',
        'player' => 'https://garr.tv/s/616e7e6a3ef1a1862024b842?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-25',
    ],
];

$talks_date = '2021-09-09';
$human_talks_date = 'Venerdi 3 Settembre';

$sponsors = [
    'Software Workers' => (object) [
        'logo' => 'https://www.ils.org/images/sponsor/softwareworkers.png',
        'link' => 'https://softwareworkers.it/',
    ],
    'Continuity' => (object) [
        'logo' => 'https://www.ils.org/images/sponsor/continuity.png',
        'link' => 'https://continuity.space/',
    ],
    'Appleby' => (object) [
        'logo' => 'https://www.ils.org/images/sponsor/appleby.png',
        'link' => 'http://www.applebyitalia.it/',
    ],
    'Linux Professional Institute' => (object) [
        'logo' => 'https://www.ils.org/images/sponsor/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
    'Ergonet' => (object) [
        'logo' => 'https://www.ils.org/images/sponsor/ergonet.png',
        'link' => 'https://www.ergonet.it',
    ],
];

$supporters = [
    'GARR' => (object) [
        'logo' => '/immagini/garr.png',
        'link' => 'https://garr.it/',
    ],
];

$patronages = [];

$theme = [];
