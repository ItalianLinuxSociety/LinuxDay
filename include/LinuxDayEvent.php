<?php
class LinuxDayEvent {

    // TODO: make private, make getters/setters
    public $owner;
    public $group;
    public $city;
    public $prov;
    public $web;
    public $coords;
    public $gadgets;
    public $gadgets_address;
    public $date;
    public $approvato;
    public $existing;

    public function __construct() {

    }

    /**
     * Get the localized date format
     * @param string $format Date format as supported by DateTime::format()
     * @return string
     */
    public function getDateLocalizedFormat($format) {
        $date = $this->getDateTime();
        if (!$date) {
            return '??';
        }
        return lol_localized_date_format($date, $format);
    }

    /**
     * Get the "Linux Day Bari" without fancy HTML tags.
     * @return string
     */
    public function getTitleNice() {
        return strip_tags($this->getHTMLTitleNice());
    }

    /**
     * Get the "Linux Day Bari" with fancy HTML tags.
     * This is safe to be printed as-is.
     * @return string
     */
    public function getHTMLTitleNice() {
        // Linux Day Bari
        // Linux Stand Bari
        return sprintf(
            "%s <b>%s</b>",
            esc_html($this->getEventFamily()),
            esc_html($this->city)
        );
    }

    public function getEventFamily() {
        // It's quite important to highlight paid events very differently lol.
        if ($this->isPaid()) {
            return "Linux Stand";
        }
        return "Linux Day";
    }

    public function getDateTime() {
        return DateTime::createFromFormat('Y-m-d', $this->getDateOrDefault());
    }

    public function getDateOrDefault() {
        if (empty($this->date)) {
            return conf('computer_date');
        }
        return $this->date;
    }

    public function isNormalLinuxDay() {
        return $this->hasTraditionalDate() && !$this->isPaid();
    }

    /**
     * Check whenever this event requires a payment to access.
     * @return bool
     */
    public function isPaid() {
        return !empty($this->paid);
    }

    /**
     * Check if this Linux Day has a traditional starting date.
     * So, does not start before, or after the expected date.
     * @return bool
     */
    public function hasTraditionalDate() {
        return $this->getDateOrDefault() === conf('computer_date');
    }

    /**
     * Check if this Linux Day has a traditional starting date.
     * So, does not start before, or after the expected date.
     * @return bool
     */
    public function hasEarlyDate() {
        return $this->getDateOrDefault() < conf('computer_date');
    }

    /**
     * Check if this Linux Day has a traditional starting date.
     * So, does not start before, or after the expected date.
     * @return bool
     */
    public function hasLateDate() {
        return $this->getDateOrDefault() > conf('computer_date');
    }

    /**
     * Render the Linux Day card
     */
    public function renderCard( $args = [] ) {
        $args['event'] = $this;
        template( 'linux-day-card', $args );
    }

    /**
     * @param stdClass|array
     * @return LinuxDayEvent
     */
    public static function createFromData($data) {
        $event = new self();
        $data = (array)$data;
        foreach ($data as $k => $v) {
            $event->{$k} = $v;
        }
        return $event;
    }

    /**
     * @param array
     * @return array
     */
    public static function createFromDataArray($rows) {
        $linux_days = [];
        foreach ($rows as $data) {
            $linux_days[] = self::createFromData($data);
        }
        return $linux_days;
    }

}
