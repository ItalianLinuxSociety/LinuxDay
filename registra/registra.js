$(document).ready(function() {
	var mymap = L.map('mapid', {
		gestureHandling: true
	}).setView([42.204, 11.711], 6);

	L.tileLayer('https://{s}.tile.openstreetmap.de/{z}/{x}/{y}.png', {
	    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
	    maxZoom: 18,
	}).addTo(mymap);

	var coordinates = $('input:hidden[name=coordinates]');
	var coords = coordinates.val().split(',');
	var marker = L.marker([parseFloat(coords[0]), parseFloat(coords[1])]).addTo(mymap);

	mymap.on('click', function(e) {
		marker.setLatLng(e.latlng);
		coordinates.val(e.latlng.lat + ',' + e.latlng.lng);
	});
});
