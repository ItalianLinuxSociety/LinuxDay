<?php

date_default_timezone_set("Europe/Rome");
$current_year = '2018';
$computer_date = '2018-10-27';
$shipping_date = '2018-10-27';
$human_date = 'Sabato 27 Ottobre 2018';
$administrators = ['bob@linux.it'];

$is_virtual = false;
$is_physical = true;
$sessions = [];
$talks_date = null;

$sponsors = [
    'Linux Professional Institute Italia' => (object) [
        'logo' => '/immagini/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
	'Crismatica' => (object) [
        'logo' => '/immagini/crismatica.png',
        'link' => 'http://www.crismatica.it/',
    ],
	'Industria Italiana Software Libero' => (object) [
        'logo' => '/immagini/iisl.png',
        'link' => 'https://www.industriasoftwarelibero.it/',
    ],
];

$supporters = [
	'Wikimedia Italia' => (object) [
        'logo' => '/immagini/wikimedia.png',
        'link' => 'http://wikimedia.it/',
    ],
];
$patronages = [];

$theme = [];
