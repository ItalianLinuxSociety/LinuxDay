<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');

fullpageheader('Linux Day ' . conf('current_year') . ': Admin');

if (empty($_SESSION['admin']) || $_SESSION['admin'] != 'S') {
	?>

	<div class="alert alert-danger">
		Questa pagina è riservata agli amministratori.
	</div>

	<?php
}
else {
	?>
	<h1 class="h1 title">Eventi Registrati</h1>
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Gruppo</th>
				<th scope="col">Città</th>
				<th scope="col">Provincia</th>
				<th scope="col">Owner</th>
				<th scope="col">Approvato</th>
				<th scope="col">Comandi</th>
			</tr>
		</thead>
		<tbody>

		<?php

		$events_file = '../data/events' . conf('current_year') . '.json';
		$events = json_decode(file_get_contents($events_file));
		foreach($events as $e) {
			?>

			<tr>
				<td><?php echo esc_html($e->group) ?></td>
				<td><?php echo esc_html($e->city) ?></td>
				<td><?php echo esc_html($e->prov) ?></td>
				<td><?php echo esc_html($e->owner) ?></td>
				<td><?php echo($e->approvato ? 'SI' : 'NO') ?></td>
				<td><a href='/<?php echo conf('current_year') ?>/registra/?owner=<?php echo esc_attr($e->owner) ?>'>Modifica evento</a> </td>
			</tr>

			<?php
		}

		?>

		</tbody>
	</table>

<?php
}

lugfooter ();
