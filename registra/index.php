<?php
/*
Copyright (C) 2019-2024  Italian Linux Society, Linux Day website contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Registra Evento',
	[makeurl('/libraries/leaflet/leaflet.css'), makeurl('/libraries/leaflet/leaflet-gesture-handling.css'), makeurl('/registra/registra.css'), makeurl('/registra/registra.css')],
	[makeurl('/libraries/leaflet/leaflet.js'), makeurl('/libraries/leaflet/leaflet-gesture-handling.js'), makeurl('/registra/registra.js')]
);

$is_admin = isset($_SESSION['admin']) && $_SESSION['admin'] == 'S';

$shipping_date = conf('shipping_date');
$past_shipping_date = false;
if ($shipping_date) {
	$past_shipping_date = date('Y-m-d') > conf('shipping_date');
}

$is_readonly_shipping = $past_shipping_date && !$is_admin;
$readonly_shipping = $is_readonly_shipping ? ' readonly' : '';

$past_save_date = date('Y-m-d') > conf('computer_date');

if ($past_save_date) {
	?>
		<div class="alert alert-danger">
			È scaduto il termine per inserire il proprio LinuxDay. Ci vediamo l'anno prossimo!
		</div>

<?php
}
if (empty($_SESSION['user_email'])) {
	?>

	<div class="alert alert-danger">
		Questa pagina è riservata a chi ha un account registrato. <a href="<?php echo makeurl('/user') ?>">Vai su questa pagina</a> per autenticarti o registrarti.
	</div>

	<?php
}
else {
	$events_file = '../data/events' . conf('current_year') . '.json';

	$owner_email = $_SESSION['user_email'];
	if ($is_admin) {
		if (isset($_REQUEST['owner']))
			$owner_email = $_REQUEST['owner'];
	}

	$event = findEvent($events_file, $owner_email);
	$message = '';

	if (isset($_POST['action'])) {
		if ($_POST['action'] == 'save') {
			$illegal_shipping = $past_shipping_date && ($event->gadgets !== isset($_POST['gadgets']) && $event->gadgets_address !== $_POST['gadgets_address']);
			$not_prov = $_POST['prov'] == '-1';

			if ($illegal_shipping || $past_save_date || $not_prov) {
				if ($not_prov) {
					$message = "<b>ERRORE:</b> Devi indicare una provincia per poter presentare il tuo evento.";
				} else {
					$message = "<b>ERRORE:</b> Passata la data entro la quale potevi presentare il tuo evento o chiedere la spedizione dei materiali.";
				}
			}
			else {
				$event->owner = $owner_email;
				$event->group = $_POST['group'];
				$event->city = $_POST['city'];
				$event->prov = $_POST['prov'];
				$event->date = $_POST['date'];
				$event->web = $_POST['web'];
				$event->coords = $_POST['coordinates'];
				$event->gadgets = isset($_POST['gadgets']);
				$event->gadgets_address = $_POST['gadgets_address'];
				$event->paid = $_POST['paid'] ?? '';
				$event->gadgets_address_name = $_POST['gadgets_address_name'];
				$event->gadgets_cap = $_POST['gadgets_cap'];
				$event->gadgets_city = $_POST['gadgets_city'] ;
				$event->gadgets_province = $_POST['gadgets_province'] ;
				$event->gadgets_phone = $_POST['gadgets_phone'] ;
				$event->is_big = $_POST['is_big'] ?? '';

				if ($is_admin) {
					if ($event->approvato == false && isset($_POST['checkbox_approvato'])) {
						$event->approvato = true;
						$text = "Grazie! L'evento che hai organizzato per il LinuxDay è stato approvato ed ora è visibile sul sito! In caso di variazioni (spostamento della sede, cancellazione dell'evento, spostamento della pagina web di riferimento...), è caldamente raccomandato aggiornare le informazioni pubblicate sul sito, o rispondere a questa mail per le opportune segnalazioni allo staff.";
						$headers = 'From: webmaster@linux.it' . "\r\n";
						mail($event->owner, 'Approvazione evento LinuxDay', $text, $headers);
					}
					else if ($event->approvato == true && !isset($_POST['checkbox_approvato'])) {
						$event->approvato = false;
					}
				}

				$result = saveEvent($events_file, $event);
				$message = 'Evento salvato correttamente! Ricorda: deve essere approvato dagli amministratori prima che compaia nella mappa in homepage.';
			}
		}
	}

	if (isset($_GET['action'])) {
		if ($_GET['action'] == 'destroy') {
			$get_email = $_GET['owner'];
			$events = json_decode(file_get_contents($events_file));

			foreach($events as $index => $e) {
				if ($e->owner == $get_email) {
					$selected = $e;
					$selected_index = $index;
					break;
				}
			}

			if ($selected->owner == $_SESSION['user_email']  || $is_admin) {
				unset($events[$selected_index]);
				$events = array_values($events);
				file_put_contents($events_file, json_encode($events));

				?>
				<p class="alert alert-info">
					Eliminato con successo. <a href="<?php echo(makeurl('/registra'))?>">Clicca qui</a> per creare un nuovo evento o clicca salva senza ricaricare se hai cancellato l'evento per sbaglio.
				</p>
				<?php
			}
			else {
				?>
				<p class="alert alert-danger">
					Devi essere admin o essere il creatore dell'evento per poterlo eliminare.
				</p>
				<?php
			}
		}
	}

	?>

	<h2>Crea / Modifica Evento</h2>

	<p class="alert alert-info">
		In caso di problemi, scrivi a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>.
	</p>

	<p class="alert alert-info">
		Si raccomanda di creare, prima della registrazione, una pagina sul proprio sito da destinare al Linux Day dell'anno corrente, da eventualmente arricchire in seguito ma per almeno annunciare data e luogo della manifestazione. Le registrazioni non vengono approvate finché sul sito linkato non si trova nessuna informazione in merito al prossimo Linux Day.<br>
		In caso di variazioni (spostamento della sede, cancellazione della manifestazione...) è caldamente raccomandato aggiornare di conseguenza l'evento qui registrato, o di contattare l'organizzazione per provvedere all'opportuno aggiornamento delle informazioni destinate al pubblico.
	</p>
	<?php if(!empty($message)): ?>
		<div class="alert alert-info">
			<?php echo $message ?>
		</div>
	<?php endif ?>

	<form method="POST" action="<?php echo makeurl('/registra/index.php') ?>">
		<input type="hidden" name="action" value="save">

		<div class="form-group">
			<label for="group">Organizzatore</label>
			<input type="text" class="form-control" id="group" name="group" value="<?php echo esc_attr($event->group) ?>" required>
			<small class="form-text text-muted">Nome del gruppo organizzatore (LUG, associazione, scuola, ente...). Non il tuo nome e cognome!</small>
		</div>
		<div class="form-group">
			<label for="city">Città</label>
			<input type="text" class="form-control" id="city" name="city" value="<?php echo esc_attr($event->city) ?>" required>
		</div>
		<div class="form-group">
			<label for="city">Provincia</label>
			<?php prov_select('form-control', $event->prov, [
				'name' => 'prov',
			] ) ?>
		</div>
		<div class="form-group">
			<label for="date">Data</label>
			<input type="text" class="form-control" id="date" name="date" placeholder="YYYY-MM-DD" value="<?php echo esc_attr($event->date ?? '') ?>">
			<small class="form-text text-muted">Data dell'evento (se diversa da quella ufficiale).</small>
		</div>
		<div class="form-group form-check">
			<input type="checkbox" class="form-check-input" id="paid" name="paid" value="1" <?= ($event->paid === '1' ? 'checked' : '') ?>>
			<label for="paid">È un evento organizzato in una fiera (e.g. dell'elettronica) con <b>ingresso a pagamento</b>?</label>
			<small class="form-text text-muted">Ricorda che il Linux Day è un evento totalmente gratuito. Quindi, se questo è un evento in una fiera a pagamento, richiederà approvazione ulteriore e dovrà essere evidenziato in modo molto chiaro sul vostro sito. Per favore chiamatelo "Linux Stand" e non "Linux Day" se l'ingresso è a pagamento.</small>
		</div>
		<div class="form-group">
			<label for="web">Sito Web</label>
			<input type="text" class="form-control" id="web" name="web" value="<?php echo esc_attr($event->web) ?>" required>
			<small class="form-text text-muted">URL della pagina su cui reperire informazioni sull'evento (e.g. http://miolug.it/linuxday). Non usate la homepage del vostro sito, piuttosto create una pagina temporanea da arricchire in seguito.</small>
		</div>
		<div class="form-group">
			<input type="hidden" name="coordinates" value="<?php echo esc_attr($event->coords) ?>">
			<label for="mapid">Luogo dell'evento</label>
			<div id="mapid"></div>
			<small class="form-text text-muted">Posiziona il marker cliccando sulla mappa, per indicare il luogo dove si svolgerà il tuo evento. Sii il più preciso possibile!</small>
		</div>
		<?php if($shipping_date): ?>

			<h3>Informazioni per spedizione gadget</h3>

			<?php if($past_shipping_date):?>
				<div class="alert alert-danger">
					È scaduto il termine per richiedere l'invio dei gadget.
				</div>
			<?php endif ?>

			<div class="form-group form-check">
				<input type="checkbox" class="form-check-input" id="gadgets" name="gadgets" <?php echo ($event->gadgets ? 'checked' : '') . $readonly_shipping ?> />
				<label class="form-check-label" for="gadgets">Richiesta di Materiale Promozionali</label>
				<small class="form-text text-muted">Ogni anno i volontari di Italian Linux Society inviano (gratuitamente) agli organizzatori del Linux Day un pacco di spille, adesivi e materialie informativo. Spunta questa casella e compila il campo sotto per riceverlo anche tu!</small>
			</div>

			<div class="form-group">
				<label for="gadgets_address">Indirizzo di recapito</label>
				<input type="text" class="form-control" id="gadgets_address" name="gadgets_address" value="<?= esc_attr($event->gadgets_address) ?>" placeholder="Via Qualcosa, 5"<?= $readonly_shipping ?> />
			</div>

			<div class="form-group">
				<label for="gadgets_address_name">Intestatario della spedizione (nome sul citofono)</label>
				<input type="text" class="form-control" id="gadgets_address_name" name="gadgets_address_name" value="<?= esc_attr($event->gadgets_address_name) ?>" placeholder="Nome Cognome"<?= $readonly_shipping ?> />
			</div>

			<div class="form-group">
				<label for="gadgets_phone">Numero di telefono della spedizione (per il corriere)</label>
				<input type="text" class="form-control" id="gadgets_phone" name="gadgets_phone" value="<?= esc_attr($event->gadgets_phone) ?>" placeholder="+395555555555"<?= $readonly_shipping ?> />
			</div>

			<div class="form-group">
				<label for="gadgets_cap">CAP della spedizione</label>
				<input type="text" class="form-control" id="gadgets_cap" name="gadgets_cap" value="<?= esc_attr($event->gadgets_cap) ?>" placeholder="00000" pattern="[0-9]{5}"<?= $readonly_shipping ?> />
			</div>
			<div class="form-group">
				<label for="gadgets_province">Provincia della spedizione</label>
				<?php prov_select('form-control', $event->gadgets_province, [
					'name' => 'gadgets_province',
					'readonly' => $is_readonly_shipping,
				] ) ?>
			</div>
			<div class="form-group">
				<label for="city">Città spedizione</label>
				<input type="text" class="form-control" id="gadgets_city" name="gadgets_city" value="<?= esc_attr($event->gadgets_city) ?>" placeholder="Città"<?= $readonly_shipping ?> />
			</div>

			<h3>Stima partecipazione</h3>

			<div class="form-group form-check">
				<input type="checkbox" class="form-check-input" id="is_big" name="is_big"<?= ($event->is_big ? ' checked' : '') ?> />
				<label class="form-check-label" for="is_big">Vi aspettate la partecipazione di più di 300 persone?</label>
				<small class="form-text text-muted">Per favore rispondi con sincerità. Se rispondi positivamente potremmo mandarti materiali di qualità relativamente inferiori ma di quantità superiori.</small>
			</div>

		<?php endif ?>

		<?php if ($is_admin): ?>
			<h3>Moderazione</h3>
			<input type="hidden" name="owner" value="<?php echo $owner_email ?>">

			<div class="form-group form-check">
				<input type="checkbox" class="form-check-input" id="checkbox_approvato" name="checkbox_approvato" <?php echo ($event->approvato ? 'checked' : '') ?>>
				<label class="form-check-label" for="checkbox_approvato">Evento approvato e da pubblicare</label>
			</div>
		<?php endif ?>

		<button type="submit" class="btn btn-primary" <?php echo($past_save_date ? 'disabled' : '') ?>>Salva</button>
	</form>
	<?php if ($event->existing !== false): ?>
		<a href="<?php echo esc_attr(makeurl('/registra/index.php?action=destroy&owner=' . $owner_email)) ?>" class="btn btn-danger">Elimina evento</a>
	<?php endif ?>

	<?php
}

lugfooter ();
