<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Promozione');

?>

<h2>Promozione</h2>

<p>
	Aiutaci anche tu a diffondere la notizia del Linux Day!
</p>
<p>
	Se hai un sito o un blog online e vuoi informare i tuoi visitatori sull'evento di <?php echo conf('human_date') ?>, usa il codice HTML sotto per includere il banner o <a href="<?php echo makeurl('/promo/' . conf('current_year') . '_webkit.zip') ?>">scarica qui l'archivio completo nei diversi formati</a>, e qua <a href="<?php echo makeurl('/immagini/linuxday_fullcolor.svg') ?>">il logo in formato SVG</a>.
</p>

<table class="table">
	<tr>
		<td>
			<p>
				<img src="<?php echo makeurl('/promo/?format=300x250') ?>" class="banner img-fluid" alt="Linux Day <?php echo conf('current_year') ?>">
			</p>

			<pre>
&lt;a href="https://www.linuxday.it"&gt;
  &lt;img src="<?php echo makeurl('/promo/?format=300x250') ?>"
  border="0" alt="Linux Day <?php echo conf('current_year') ?>" /&gt;
&lt;/a&gt;
</pre>
		</td>
	</tr>
</table>

<?php
lugfooter ();
