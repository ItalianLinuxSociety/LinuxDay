<?php

require_once ('funzioni.php');

$csv = '';

if (file_exists($events_file)) {
	$date = conf('computer_date');
	$events = json_decode(file_get_contents($events_file));

	foreach($events as $event) {
		if ($event->approvato) {
			list($lat, $lon) = explode(',', $event->coords);
			$csv .= sprintf("LAT=%s;LON=%s;country=Italy;city=%s;presenter=%s;presentation=LPD.%d.2;link_presenter=%s;event-date=%s;ICON=green;remark=;needs=;promise=ACCEPT\n", $lat, $lon, $event->city, $event->group, substr($date, 0, 4), $event->web, $date);
		}
	}
}

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=italy.csv");
header("Pragma: no-cache");
header("Expires: 0");
echo $csv;

