<?php
/*
Copyright (C) 2022  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');

$slug = $_GET['slug'] ?? '';
$session = findSession($slug);
if (is_null($session)) {
    header('Location: index.php');
}

fullpageheader('Linux Day ' . conf('current_year') . ' | ' . $session->label);

$found = false;
$days = json_decode(file_get_contents($days_file));
foreach($days as $day) {
    $others = [];

    foreach($day->columns as $index => $column) {
        if ($column == $slug) {
            $target_schedule_day = $day;
            $target_schedule_index = $index;
            $found = true;
        }
        else {
            $others[] = $column;
        }
    }

    if ($found) {
        break;
    }
}

?>

<div class="row mb-5">
    <div class="col-sm-12 col-md-4">
        <h2><?php echo $session->label ?></h2>
    </div>
    <div class="col-sm-12 col-md-8">
        <a href="index.php" class="btn btn-sm btn-info" id="back-to-schedule">Torna al programma completo</a>

        <?php foreach($others as $o): ?>
            <a href="live.php?slug=<?php echo $o ?>" class="btn btn-sm btn-warning">Vai a <?php echo findSession($o)->label ?></a>
        <?php endforeach ?>
    </div>
</div>

<div class="row row-eq-height">
    <div class="col-sm-12 col-md-4 order-sm-2 order-md-1">
        <iframe id="embedded-chat" src="https://chat.linux.it/channel/linuxday-sessione<?php echo $target_schedule_index + 1 ?>?layout=embedded" width="100%" height="100%" style="min-height:400px"></iframe>
    </div>
    <div class="col-sm-12 col-md-8 order-sm-1 order-md-2">
        <?php if($session->live): ?>
            <div style="position:relative;padding-top:56.25%;">
                <iframe src="<?php echo $session->player ?>" style="position:absolute;top:0;left:0;width:99%;height:99%;" frameborder="0" allow="autoplay; fullscreen;" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
            </div>
        <?php else: ?>
            <div class="text-center alert alert-info">
                <?php if(date('Y-m-d') > conf('computer_date')): ?>
                    Le registrazioni di questa sessione saranno pubblicate nei prossimi giorni: seguici <a href="https://twitter.com/linuxdayitalia">su Twitter</a>, <a href="https://www.facebook.com/LinuxDayItalia">su Facebook</a> o <a href="https://www.ils.org/newsletter">sulla newsletter</a> per aggiornamenti!
                <?php else: ?>
                    Torna su questa pagina <?php echo conf('human_date') ?> per seguire la diretta streaming, e seguici <a href="https://twitter.com/linuxdayitalia">su Twitter</a>, <a href="https://www.facebook.com/LinuxDayItalia">su Facebook</a> o <a href="https://www.ils.org/newsletter">sulla newsletter</a> per aggiornamenti!
                <?php endif ?>
            </div>
        <?php endif ?>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-sm-12 col-md-4 order-2 order-md-1">
        <table class="table table-bordered schedule">
            <thead>
                <tr>
                    <th></th>
                    <th>Programma della Sessione</th>
                </tr>
            </thead>
            <tbody>
                <?php unrollDay($target_schedule_day->events, $target_schedule_index, false) ?>
            </tbody>
        </table>
    </div>

    <div class="col-sm-12 col-md-8 order-1 order-md-2">
        <div class="row">
            <div class="col">
                <p class="border p-2">
                    <a href="https://www.linux.it/" target="_blank">Scopri di più su Linux e sul software libero</a>.
                    <br><br>
                    <a href="https://www.linux.it/stickers" target="_blank">Richiedi gli adesivi gratuiti</a> distribuiti da Italian Linux Society.
                </p>
            </div>
            <div class="col text-right">
                <a href="https://www.ils.org/sostieni" target="_blank" class="float-end">
                    <img src="https://www.ils.org/assets/images/sostieni_ils.png" style="max-width: 100%">
                </a>
            </div>
        </div>
    </div>
</div>

<?php

lugfooter();
