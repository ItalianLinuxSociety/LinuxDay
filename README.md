# Linux Day (linuxday.it)

Questo è il codice sorgente del sito web [Linux Day](https://www.linuxday.it/), la principale conferenza annuale dedicata al software libero e all'open source in Italia.

https://www.linuxday.it/

## Utilizzo del Repository in Locale

Questo repository è pensato per essere esposto come un singolo `/anno/`, per motivi storici.

In questo modo è molto facile in produzione che altri anni siano implementati in modo diverso. Ma è comunque decisamente facile riciclare molto codice per ogni nuovo anno.

Esempio. Per ottenere l'indirizzo http://localhost/2024/ devi avere una struttura del genere in una cartella del tuo computer:

```
DocumentRoot
  2024 -> /percorso/al/repository/linuxday
```

Esempio. L'ambiente di produzione è approssimabile in locale eseguendo questi comandi:

```
git clone https://gitlab.com/ItalianLinuxSociety/linuxday.git linuxday
mkdir LinuxDayDocumentRoot
echo "<?php header('Location: ' . date('Y'));" > LinuxDayDocumentRoot/index.php
ln -s ../linuxday LinuxDayDocumentRoot/2022
ln -s ../linuxday LinuxDayDocumentRoot/2023
ln -s ../linuxday LinuxDayDocumentRoot/2024
cd LinuxDayDocumentRoot
```

A questo punto, dalla cartella principale che contiene tutti gli anni (`LinuxDayDocumentRoot`), puoi eseguire il webserver di sviluppo, per fare test:

```
php -S localhost:8080
```

E puoi testare il tuo anno:

http://localhost:8080/2024/


## Accesso amministratore

Per accedere come amministratore devi registrarti con un indirizzo email presente nel file `config.<anno>.php` nell'array `$administrators`, oppure semplicemente puoi rinominare `/data/users.txt.sample` in `/data/users.txt`, che è predisposto per accedere a http://localhost:8080/2024/admin con:
- **username:** direttore@linux.it
- **password:** admin

In questo caso l'indirizzo email che si autentica deve essere sempre in `/config.<anno>.php`.


## Contribuire

Per contribuire allo sviluppo di questo progetto, dopo aver effettuato i passaggi in alto, visita la pagina del progetto e clicca sul tasto "Fork":

https://gitlab.com/ItalianLinuxSociety/linuxday

A questo punto dovresti poter fare `git commit` in locale, e fare `git push` nella tua fork.

Facendo "git push" nella tua Fork, GitLab ti suggerirà di aprire una "Merge Request".

Dubbi? Chiarimenti? Contattaci!

https://www.ils.org/contatti/

Grazie per le tue idee e i tuoi contributi!

## Licenza

Questo progetto ed ogni suo contributo è rilasciato dai contributori del sito LinuxDay.it attraverso la licenza GNU Affero General Public License.

https://www.gnu.org/licenses/agpl-3.0.html

Puoi fare:

* utilizzare questo software
* modificare questo software
* condividere questo software e le tue modifiche

Per qualsiasi scopo, anche per scopo commerciale, a patto di:

* rilasciare il tuo progetto e ogni tua modifica con le stesse libertà

Grazie mille per i tuoi contributi!

## Crediti

Il repository è stato ideato da Roberto Guido.

Grazie ad ogni persona che ha contribuito al sito:

https://gitlab.com/ItalianLinuxSociety/linuxday/-/graphs/master
