<?php

date_default_timezone_set("Europe/Rome");
$current_year = '2015';
$computer_date = '2015-10-24';
$shipping_date = '2015-10-24';
$human_date = 'Sabato 24 Ottobre 2015';
$administrators = ['bob@linux.it'];

$is_virtual = false;
$is_physical = true;
$sessions = [];
$talks_date = null;

$sponsors = [
	'Bluemix' => (object) [
        'logo' => '/immagini/bluemix.png',
    	'link' => 'https://console.ng.bluemix.net/',
    ],
    'Linux Professional Institute Italia' => (object) [
        'logo' => '/immagini/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
	'Studio Storti' => (object) [
        'logo' => '/immagini/storti.png',
    	'link' => 'http://studiostorti.com/',
    ],
	'Carrara Computing International' => (object) [
        'logo' => '/immagini/cci.png',
    	'link' => 'http://www.ccinetwork.it/',
    ],
];

$supporters = [];
$patronages = [];

$theme = [];
