<?php

require_once ('../funzioni.php');

if (file_exists($days_file)) {
    $days = json_decode(file_get_contents($days_file));

    ?>

    <?php foreach($days as $day): ?>
        <hr/>
        <div class="alert alert-warning text-center">
            <h3 class="mt-5 mb-5"><?php echo $day->title ?></h3>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered schedule">
                <thead>
                    <tr>
                        <th width="10%"></th>
                        <?php foreach($day->columns as $column): ?>
                            <th width="<?php echo round(90 / count($day->columns), 2) ?>%" style="min-width: 180px">
                                <!--
                                <a href="<?php echo makeurl('programma/live.php?slug=' . $column) ?>"><?php echo findSession($column)->label ?><br><small>Clicca qui per accedere</small></a>
                                -->
                                <?php echo findSession($column)->label ?>
                            </th>
                        <?php endforeach ?>
                    </tr>
                </thead>
                <tbody>
                    <?php unrollDay($day->events, -1, false) ?>
                </tbody>
            </table>
        </div>
    <?php endforeach ?>
<?php } ?>
